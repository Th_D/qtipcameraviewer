#include "ipCameraViewerWindow.h"
#include "ui_ipCameraViewerWindow.h"


#include <QLabel>
#include <QPixmap>



#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <iostream>
#include <unistd.h>

#define TEXT_PAUSE QString("Pause")
#define TEXT_UNPAUSE QString("Unpause")



ipCameraViewerWindow::ipCameraViewerWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ipCameraViewerWindow)
{
    ui->setupUi(this);

    movieLoading = new QMovie("loader.gif", QByteArray(), this);
    pixmap = this->findChild<QLabel *>(QString("lbl_video"));
    btnExit = this->findChild<QPushButton*>("pb_exit");
    btnPause = this->findChild<QPushButton*>("pb_pause");
    rbWebCam = this->findChild<QRadioButton*>("rb_webcam");
    rbIP = this->findChild<QRadioButton*>("rb_IP");
    cbIP = this->findChild<QComboBox*>("cb_IP");
    leIP = this->findChild<QLineEdit*>("le_IP");

    leIP->setEnabled(false);
    rbWebCam->setChecked(true);

    cbIP->addItem("Others");
    cbIP->addItem("http://93.63.152.227:85/mjpg/video.mjpg");
    cbIP->addItem("http://187.157.229.132:80/mjpg/video.mjpg");
    cbIP->addItem("http://176.57.73.231:80/mjpg/video.mjpg");

    videoThread = new ThreadVideo("0");
    videoThread->start();

    connect(videoThread, SIGNAL(imgReady(QImage, int, int)), this, SLOT(onImgReady(QImage, int, int)));
    connect(btnExit, SIGNAL(pressed()), this, SLOT(close()));
    connect(btnPause, SIGNAL(pressed()), this, SLOT(onPausePressed()));
    connect(btnPause, SIGNAL(pressed()), videoThread, SLOT(onPause()));
    connect(rbWebCam, SIGNAL(toggled(bool)), this, SLOT(onWCchecked(bool)));
    connect(rbIP, SIGNAL(toggled(bool)), this, SLOT(onIPchecked(bool)));
    connect(cbIP, SIGNAL(currentIndexChanged(int)), this, SLOT(onIPChanged()));
    connect(leIP, SIGNAL(textChanged(QString)), this, SLOT(onIPChanged()));
}

void ipCameraViewerWindow::onImgReady(QImage img, int width, int height){
    pixmap->setFixedHeight((pixmap->width()*height)/width);
    pixmap->setPixmap( QPixmap::fromImage(img) );
}

void ipCameraViewerWindow::onPausePressed()
{
    if(btnPause->text() == TEXT_PAUSE)
        btnPause->setText(TEXT_UNPAUSE);
    else
        btnPause->setText(TEXT_PAUSE);
}

void ipCameraViewerWindow::onWCchecked(bool b)
{
    if(b)
    {
        videoThread->setSrc("0");
        leIP->setEnabled(false);
        leIP->setText("");
        cbIP->setEnabled(false);
    }
}

void ipCameraViewerWindow::onIPchecked(bool b)
{
   if(b)
   {
       cbIP->setEnabled(true);
       if(cbIP->currentText() == "Others"){
           leIP->setEnabled(true);
           videoThread->setSrc(leIP->text());
       } else {
           videoThread->setSrc(cbIP->currentText());
       }
   }
}

void ipCameraViewerWindow::onIPChanged()
{
    std::cout<<"IP changed"<<std::endl;
    QString selection = cbIP->currentText();
    if(selection == "Others"){
        leIP->setEnabled(true);
        videoThread->setSrc(leIP->text());
    }
    else {
        leIP->setEnabled(false);
        videoThread->setSrc(cbIP->currentText());
        leIP->setText("");
    }
}

ipCameraViewerWindow::~ipCameraViewerWindow()
{
    delete ui;
}
