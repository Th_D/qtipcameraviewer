#ifndef THREADVIDEO_H
#define THREADVIDEO_H

#include <QThread>
#include <QLabel>
#include <QImage>
#include <QPixmap>
#include <QMutex>
#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <iostream>
#include <unistd.h>

class ThreadVideo : public QThread
{
    Q_OBJECT

private:
    QLabel* pixmap;
    cv::VideoCapture vcap;
    cv::Mat image;
    bool pause;
    bool finito;
    bool changing;
    QMutex mutex;

signals:
     void imgReady(QImage, int, int);

public slots:
     void onPause();

public:
    ThreadVideo(QString);
    void setSrc(QString);
    void run();
    void close();
};

#endif // THREADVIDEO_H

