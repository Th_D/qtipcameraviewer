# QtIpCameraViewer
### IP camera viewer
---
```
Compile with :

* `qmake`
* `make` 

---

To use : 

* `./ip_video_viewer`
* you can disp video from your webcam
*  or from the net, by selecting IP and a proposed IP or a link to a mjpg stream

---

Developped whith QtCreator, 
using Qt 4.8, opencv-2.4.16.6
Tested with IP found at https://www.insecam.org/
