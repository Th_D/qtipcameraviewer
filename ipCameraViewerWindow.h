#ifndef IPCAMERAVIEWERWINDOW_H
#define IPCAMERAVIEWERWINDOW_H

#include <QMainWindow>
#include <QThread>
#include <QLabel>
#include <QPixmap>
#include <QPushButton>
#include <QRadioButton>
#include <QComboBox>
#include <QLineEdit>
#include <QMovie>

#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <iostream>
#include <unistd.h>
#include "threadvideo.h"

namespace Ui {
class ipCameraViewerWindow;
}

class ipCameraViewerWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit ipCameraViewerWindow(QWidget *parent = 0);
    ThreadVideo* videoThread;

    ~ipCameraViewerWindow();

private:
    QLabel *pixmap;
    Ui::ipCameraViewerWindow *ui;
    QPushButton* btnPause;
    QPushButton* btnExit;
    QRadioButton* rbWebCam;
    QRadioButton* rbIP;
    QComboBox* cbIP;
    QLineEdit* leIP;
    QMovie* movieLoading;

public slots:
    void onImgReady(QImage, int, int);
    void onPausePressed();
    void onWCchecked(bool b);
    void onIPchecked(bool b);
    void onIPChanged();

};


#endif // IPCAMERAVIEWERWINDOW_H
