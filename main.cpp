#include "ipCameraViewerWindow.h"
#include <QApplication>
#include <QFuture>
#include <opencv2/opencv.hpp>

using namespace QtConcurrent;

int main(int argc, char *argv[])
{
    std::cout<<cv::getBuildInformation()<<std::endl;
    QApplication a(argc, argv);
    ipCameraViewerWindow w;
    w.show();
    return a.exec();
}
