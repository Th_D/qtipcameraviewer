#include "threadvideo.h"

ThreadVideo::ThreadVideo(QString ip)
{
    std::cout << "\tCONSTRUCTOR : "<< ip.toStdString() << std::endl;
    if (ip == "0")
    {
        if(!vcap.open(0)) {
            std::cout << "Error opening video stream from webcam" << std::endl;
            return ;
        }
        else
            std::cout << "Open video stream from webcam" << std::endl;
    }
    else
    {
        if(!vcap.open(ip.toStdString())) {
            std::cout << "Error opening video stream from " << ip.toStdString() << std::endl;
            return ;
        }
        else
            std::cout << "Open video stream from " << ip.toStdString() << std::endl;
    }
    pause = false;
}

void ThreadVideo::run() {
    while(true)
    {
        mutex.lock();
        if(!vcap.read(image)) {
           // std::cout << "No frame" << std::endl;
        }
        mutex.unlock();

        if(!image.empty())
        {
            QImage qimg(image.data, image.cols, image.rows, image.step, QImage::Format_RGB888);

            if(!pause)
                emit(imgReady(qimg.rgbSwapped(), qimg.width(), qimg.height()));
        }

    }
}

void ThreadVideo::setSrc(QString src){

    if (src == "0")
    {
        mutex.lock();
        if(!vcap.open(0)) {
            mutex.unlock();
            std::cout << "Error opening video stream from webcam" << std::endl;
            return ;
        }
        else
            std::cout << "Open video stream from webcam" << std::endl;
        mutex.unlock();
    }
    else
    {
        mutex.lock();
        if(!vcap.open(src.toStdString())) {
            mutex.unlock();
            std::cout << "Error opening video stream from " << src.toStdString() << std::endl;
            return ;
        }
        else
            std::cout << "Open video stream from " << src.toStdString() << std::endl;
        mutex.unlock();
    }
}

void ThreadVideo::onPause()
{
    pause = !pause;
}
